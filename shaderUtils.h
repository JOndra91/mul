/*
 * File: shaderUtils.h
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *
 */

#ifndef SHADERUTILS_HPP
#define    SHADERUTILS_HPP


#include <stdio.h>
#include <GL/glew.h>

#ifdef    __cplusplus
extern "C"
{
#endif

    typedef enum
    {
        ERR_NO_FILE,
        ERR_VERTEX_SHADER,
        ERR_FRAGMENT_SHADER,
        ERR_COMPUTE_SHADER,
        ERR_PROGRAM_LINK
    } t_ShaderUtilsError;

    extern t_ShaderUtilsError shaderUtilsErrno;
    GLuint createShaderFromFile(const char *sourceFile, GLenum type);
    GLuint createShaderFromSource(const char *source, GLenum type);
    GLuint compileProgramFromFile(const char *vertexShaderFile, const char *fragmentShaderFile);
    GLuint compileProgramFromSource(const char *vertexShaderSource, const char *fragmentShaderSource);
    GLuint compileComputeProgramFromFile(const char *computeShaderFile);
    GLuint compileComputeProgramFromSource(const char *source);
    void shaderUtilsPrintError(char *info);
    void shaderUtilsPrintGLError(GLuint object);
    unsigned char* getFileContent(const char* path, unsigned long *length);


#ifdef    __cplusplus
}
#endif

#endif    /* SHADERUTILS_HPP */

