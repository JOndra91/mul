/*
 * File: jreadMemory.c
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#include <malloc.h>
#include <string.h>

#include "jreadMacros.h"
#include "jreadMemory.h"

#define MEM_POINTER(memory, reader) ((memory) = (jrMem*)((reader)->_source))

typedef struct {
    uint8 *start;
    uint8 *end;
    uint8 *position;
} jrMem;

static int jrMemRead(jread *reader, void *dest, size_t n, size_t *read);
static int jrMemReadWord(jread *reader, uint16 *dest);
static int jrMemSeek(jread *reader, size_t offset);
static void jrMemClose(jread *reader);
static void jrMemInfo(jread *reader, size_t *curOffset, size_t *size);

int jreadOpenMemory(jread *reader, void *memory, size_t size) {
    jrMem *m;

    jreadInit(reader);

    if ((m = (jrMem*) malloc(sizeof (jrMem))) == NULL) {
        JR_THROW(reader, JR_OUT_OF_MEMORY);
    }

    m->start = m->position = memory;
    m->end = memory + size;

    reader->_source = m;
    reader->_read = jrMemRead;
    reader->_readWord = jrMemReadWord;
    reader->_seek = jrMemSeek;
    reader->_info = jrMemInfo;
    reader->_close = jrMemClose;

    return JR_OK;
}

int jrMemRead(jread *reader, void *dest, size_t n, size_t *read) {
    jrMem *m;

    MEM_POINTER(m, reader);

    if (m->position + n > m->end) {
        n = m->end - m->position;
    }
    else if (m->position == m->end) {
        JR_THROW(reader, JR_EOF);
    }

    if (n < 8) {
        int i;
        for (i = 0; i < n; i++) {
            *((uint8*) dest + i) = *(m->position + i);
        }
    }
    else {
        memcpy(dest, m->position, n);
    }

    m->position += n;

    if (read) {
        *read = n;
    }

    return JR_OK;
}

int jrMemSeek(jread *reader, size_t offset) {
    jrMem *m;
    uint8 *p;

    MEM_POINTER(m, reader);

    p = m->position + offset;

    if (p >= m->start && p < m->end) {
        m->position = p;
        return JR_OK;
    }

    JR_THROW(reader, JR_EOF);
}

int jrMemReadWord(jread *reader, uint16 *dest) {
    jrMem *m;

    MEM_POINTER(m, reader);

    if (m->position + 2 > m->end) {
        JR_THROW(reader, JR_EOF);
    }

    *dest = m->position[0] << 8 | m->position[1];

    m->position += 2;

    return JR_OK;
}

void jrMemInfo(jread* reader, size_t* currOffset, size_t* size) {
    jrMem *m;

    MEM_POINTER(m, reader);

    if (currOffset) {
        *currOffset = m->start - m->position;
    }

    if (size) {
        *size = m->end - m->start;
    }
}

void jrMemClose(jread* reader) {
    free(reader->_source);
}