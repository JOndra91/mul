/*
 * File: jreadMarkers.c
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#include <stdlib.h>
#include <string.h>
#include "jreadMacros.h"
#include "jreadTables.h"
#include "jreadMarkers.h"
#include "jreadDecoders.h"

int jreadGetQntTable(jread* reader) {
    int i;
    uint16 *qntTbl;
    uint16 precision, destination, len;
    int length;
    size_t read;

    JR_TRY(reader->_readWord(reader, &len));
    length = len - 2;
    CLEAR_BUFFER(reader);
    FILL_BUFFER(reader, length, &read);

    do {

        --length;

        if (length < DCTSIZE2) {
            JR_THROW(reader, JR_UNEXPECTED);
        }

        READ_BYTE(reader, destination);
        precision = HIGH_HALF_BYTE(destination);
        destination = LOW_HALF_BYTE(destination);

        qntTbl = (uint16*) malloc(sizeof (uint16) * DCTSIZE2);

        if (precision != 0) {
            JR_THROW(reader, JR_UNSUPPORTED);
        }

        for (i = 0; i < DCTSIZE2; i++) {
            READ_BYTE(reader, qntTbl[zigZagRevOrd[i]]);
        }

        reader->qntTable[destination] = qntTbl;

        length -= DCTSIZE2;

    }
    while (length != 0);

    return JR_OK;
}

int jreadGetHuffTable(jread *reader) {
    uint16 tmp;
    uint8 class;
    uint16 destination;
    int i, length, cnt;
    jrHuffTbl *data;
    size_t read;

    JR_TRY(reader->_readWord(reader, &tmp));
    length = tmp - 2;
    CLEAR_BUFFER(reader);
    FILL_BUFFER(reader, length, &read);

    do {

        cnt = 0;

        READ_BYTE(reader, class);
        destination = LOW_HALF_BYTE(class);
        class = HIGH_HALF_BYTE(class) & 0x01;

        --length;

        data = (jrHuffTbl*) malloc(sizeof (jrHuffTbl));
        memset(data, 0, sizeof (jrHuffTbl));

        if (length < 16) {
            JR_THROW(reader, JR_UNEXPECTED);
        }

        for (i = 0; i < 16; i++) {
            READ_BYTE(reader, tmp);
            data->bits[i] = tmp;
            cnt += tmp;
        }

        length -= 16;

        if (length < cnt || cnt >= 256) {
            JR_THROW(reader, JR_UNEXPECTED);
        }

        for (i = 0; i < cnt; i++) {
            READ_BYTE(reader, data->vals[i]);
        }

        length -= cnt;

        if (class == 0) {
            reader->huffTables.dc[destination] = data;
        }
        else {
            reader->huffTables.ac[destination] = data;
        }

    }
    while (length != 0);

    return JR_OK;
}

int jreadGetRstDef(jread *reader) {
    uint16 interval;

    JR_TRY(reader->_readWord(reader, &interval));

    if (interval != 4) {
        return JR_ERR;
    }

    JR_TRY(reader->_readWord(reader, &interval));

    reader->restartIntervals = interval;

    return JR_OK;

}

int jreadGetNumLines(jread *reader) {
    uint16 lines;

    JR_TRY(reader->_readWord(reader, &lines));

    if (lines != 4) {
        return JR_ERR;
    }

    JR_TRY(reader->_readWord(reader, &lines));

    reader->numberOfLines = lines;

    return JR_OK;
}

int jreadSkip(jread* reader) {
    uint16 length;

    JR_TRY(reader->_readWord(reader, &length));
    return reader->_seek(reader, length - 2);
}

int jreadGetFrame0(jread *reader) {
    int i;
    unsigned int maxVerticalSampling = 0, maxHorizontalSampling = 0;
    uint16 length;
    jrFrame *frame;
    jrComp *c;
    size_t read;

    if ((frame = malloc(sizeof (jrFrame))) == NULL) {
        JR_THROW(reader, JR_OUT_OF_MEMORY);
    }

    reader->frame = frame;

    memset(frame, 0, sizeof (jrFrame));

    reader->_readWord(reader, &length);

    CLEAR_BUFFER(reader);
    FILL_BUFFER(reader, length - 2, &read);

    READ_BYTE(reader, frame->precision);

    if (frame->precision != 8) {
        JR_THROW(reader, JR_UNSUPPORTED);
    }

    READ_WORD(reader, frame->height);
    READ_WORD(reader, frame->width);

    READ_BYTE(reader, frame->numOfComponents);

    for (i = 0; i < frame->numOfComponents; i++) {
        c = frame->component + i;
        READ_BYTE(reader, c->index);

        READ_BYTE(reader, c->sampX);

        c->sampY = LOW_HALF_BYTE(c->sampX);
        c->sampX = HIGH_HALF_BYTE(c->sampX);

        READ_BYTE(reader, c->qntTableIndex);

        if (c->sampY > maxVerticalSampling) {
            maxVerticalSampling = c->sampY;
        }

        if (c->sampX > maxHorizontalSampling) {
            maxHorizontalSampling = c->sampX;
        }

        c->dcPredict = 0;
    }

    frame->maxSampX = maxHorizontalSampling;
    frame->maxSampY = maxVerticalSampling;
    frame->mcuHeight = maxVerticalSampling * DCTSIZE;
    frame->mcuWidth = maxHorizontalSampling * DCTSIZE;
    frame->heightInMcu = DIV_ROUND_UP(frame->height, frame->mcuHeight);
    frame->widthInMcu = DIV_ROUND_UP(frame->width, frame->mcuWidth);
    frame->heightInBlocks = DIV_ROUND_UP(frame->height, DCTSIZE);
    frame->widthInBlocks = DIV_ROUND_UP(frame->width, DCTSIZE);

    for (i = 0; i < frame->numOfComponents; i++) {
        c = frame->component + i;
        c->heightInBlocks = frame->heightInMcu * c->sampY;
        c->widthInBlocks = frame->widthInMcu * c->sampX;
        c->scanlineSize = c->widthInBlocks * DCTSIZE;
    }

    return JR_OK;
}