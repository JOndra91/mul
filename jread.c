/*
 * File: jread.c
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#include <string.h>
#include "jread.h"
#include "jreadDecoders.h"
#include "jreadMarkers.h"
#include "jreadMacros.h"

char *jrErrStr[] = {
    "",
    "Error",
    "Out of memory",
    "Unexpected input",
    "Unsupported format",
    "End of file"
};

void jreadInit(jread *reader) {
    memset(reader, 0, sizeof (jread));

    reader->error = JR_OK;
    reader->_buffPtr = malloc(JR_BUFFER_SIZE);
    reader->buff = reader->_buffPtr;
    reader->_toPhase = JR_PHASE_ALL;
}

int jreadGetHeaders(jread* reader) {
    int err;
    unsigned short marker;

    err = JR_OK;

    JR_TRY(reader->_readWord(reader, &marker));
    if (marker != JR_SOI) {
        return JR_ERR;
    }

    while (err == JR_OK && reader->_readWord(reader, &marker) == JR_OK) {
        switch (marker) {
        case JR_DQT:
            err = jreadGetQntTable(reader);
            break;

        case JR_DHT:
            err = jreadGetHuffTable(reader);
            break;

        case JR_DRI:
            err = jreadGetRstDef(reader);
            break;

        case JR_DNL:
            err = jreadGetNumLines(reader);
            break;

        case JR_SOF0:
            err = jreadGetFrame0(reader);
            break;

        case JR_SOS:
            //reader->seek(reader, -2);
            return JR_OK;

        case JR_COM:
            err = jreadSkip(reader);
            break;

        default:
            if (marker >= 0xffe0 && marker <= 0xffef) {
                // APPn and JPGn markers
                err = jreadSkip(reader);
            }
            else {
                JR_THROW(reader, JR_UNSUPPORTED);
            }
        }
    }

    return JR_ERR;
}

int jreadDecode(jread* reader) {

    if (reader->error != JR_OK) {
        return JR_ERR;
    }

    JR_TRY(jreadDecodeScan(reader));

    if (reader->_toPhase >= JR_PHASE_CONVERT) {
        JR_TRY(jreadConvert(reader));
    }


    return JR_OK;

}

void jreadFree(jread* reader) {
    int i;
    reader->_close(reader);
    //free(reader->_source); <- This should be handled in reader->_close
    for (i = 0; i < NUM_HUFF_TABLES; i++) {
        if (reader->huffTables.ac[i]) {
            free(reader->huffTables.ac[i]->tree);
            free(reader->huffTables.ac[i]->lookupTable);
        }
        if (reader->huffTables.dc[i]) {
            free(reader->huffTables.dc[i]->tree);
            free(reader->huffTables.dc[i]->lookupTable);
        }
        free(reader->huffTables.ac[i]);
        free(reader->huffTables.dc[i]);
    }
    for (i = 0; i < NUM_QNT_TABLES; i++) {
        free(reader->qntTable[i]);
    }
    if (reader->frame) {
        for (i = 0; i < reader->frame->numOfComponents; ++i) {
            free(reader->frame->component[i].bitmap);
            free(reader->frame->component[i].coef);
        }
    }
    free(reader->blockMap);
    free(reader->blocks);
    free(reader->frame);
    free(reader->_buffPtr);
    free(reader->image);
}