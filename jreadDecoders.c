/*
 * File: jreadDecoders.c
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "jreadMacros.h"
#include "jreadTables.h"
#include "jreadDecoders.h"

static inline int jreadPrepareHuffTable(jread *reader, jrHuffTbl *table) {

    register unsigned int i, j, k;
    register uint8 *vals;
    register int range, cnt;
    jrHuffLookup *lookup;
    register jrHuffLookup *lptr, lookupEntry;

    lookup = table->lookupTable;

    if (lookup == NULL) {
        if ((lookup = malloc(sizeof (jrHuffLookup) * HUFF_LOOKUP_SIZE)) == NULL) {
            JR_THROW(reader, JR_OUT_OF_MEMORY);
        }
    }

    memset(lookup, 0, sizeof (jrHuffLookup) * HUFF_LOOKUP_SIZE);

    lptr = lookup;
    vals = table->vals;

    range = HUFF_LOOKUP_SIZE;
    for (i = 0; i < HUFF_LOOKUP_BITS; ++i) {
        range >>= 1;
        cnt = table->bits[i];
        if (cnt == 0) {
            continue;
        }

        lookupEntry.length = i + 1;

        for (j = 0; j < cnt; ++j) {
            lookupEntry.code = *vals;
            ++vals;
            for (k = 0; k < range; ++k) {
                *lptr = lookupEntry;
                ++lptr;
            }
        }
    }

    table->lookupTable = lookup;
    table->prepared = JR_TRUE;

    return JR_OK;

}

#define PREPARE_BITS(huff,n) do { \
    register uint8 word; \
    register uint8 marker; \
    while ((n) > (huff)->nbit) { \
        if(BUFFER_SIZE(reader) < 2) { \
            if(BUFFER_SIZE(reader) == 0) { \
                CLEAR_BUFFER(reader); \
            } else { \
                (reader)->_buffPtr[0] = (reader)->buff[0]; \
            } \
            (reader)->buff = (reader)->_buffPtr + BUFFER_SIZE(reader); \
            FILL_BUFFER(reader, JR_BUFFER_SIZE - BUFFER_SIZE(reader), &read); \
            (reader)->buff = (reader)->_buffPtr; \
        } \
        READ_BYTE(reader, word); \
\
        if (word == JR_MARKER) { \
            READ_BYTE(reader, marker); \
            switch (marker) { \
                case 0x00: \
                case 0xff: \
                    break; \
                default: \
                    if (marker == 0xd9 || (marker & 0xf8) == 0xd0) { \
                        word = 0; \
                        (reader)->buff -= 2; \
                                } \
                    else { \
                        JR_THROW(reader, JR_UNEXPECTED); \
                                } \
                    } \
        } \
\
        (huff)->bits = (huff)->bits << 8 | word; \
        (huff)->nbit += 8; \
    } \
} \
while (0)

#define GET_BITS(huff,nbits) (((huff)->bits >> ((huff)->nbit - (nbits))) & ((1 << nbits) - 1))

#define HUFF_DECODE(tbl, huff) do { \
    size_t read; \
    register int bits; \
    register int16 value = 0; \
    register jrHuffLookup lookup;\
    PREPARE_BITS((huff),16); \
    lookup = (tbl)->lookupTable[(uint16) GET_BITS((huff),16)]; \
    if (lookup.length == 0) { \
        JR_THROW(reader, JR_UNEXPECTED); \
    } \
    (huff)->nbit -= lookup.length; \
    (huff)->code = lookup.code; \
    bits = lookup.code & 0x0f; \
    if (bits != 0) { \
        PREPARE_BITS((huff),bits); \
        value = (int16) GET_BITS((huff),bits); \
        (huff)->nbit -= bits; \
        if (value < (1 << (bits - 1))) { \
            value += ((-1) << bits) + 1; \
        } \
    } \
    (huff)->value = value; \
} while(0)


#define W1 2841
#define W2 2676
#define W3 2408
#define W5 1609
#define W6 1108
#define W7 565

static inline void jrRowIdct(int16 *input, int16 *output) {
    int x0, x1, x2, x3, x4, x5, x6, x7, x8;
    if (!((x1 = input[4] << 11)
        | (x2 = input[6])
        | (x3 = input[2])
        | (x4 = input[1])
        | (x5 = input[7])
        | (x6 = input[5])
        | (x7 = input[3]))) {
        output[0] = output[1] = output[2] = output[3] = output[4] = output[5] = output[6] = output[7] = input[0] << 3;
        return;
    }
    x0 = (input[0] << 11) + 128;
    x8 = W7 * (x4 + x5);
    x4 = x8 + (W1 - W7) * x4;
    x5 = x8 - (W1 + W7) * x5;
    x8 = W3 * (x6 + x7);
    x6 = x8 - (W3 - W5) * x6;
    x7 = x8 - (W3 + W5) * x7;
    x8 = x0 + x1;
    x0 -= x1;
    x1 = W6 * (x3 + x2);
    x2 = x1 - (W2 + W6) * x2;
    x3 = x1 + (W2 - W6) * x3;
    x1 = x4 + x6;
    x4 -= x6;
    x6 = x5 + x7;
    x5 -= x7;
    x7 = x8 + x3;
    x8 -= x3;
    x3 = x0 + x2;
    x0 -= x2;
    x2 = (181 * (x4 + x5) + 128) >> 8;
    x4 = (181 * (x4 - x5) + 128) >> 8;
    output[0] = (x7 + x1) >> 8;
    output[1] = (x3 + x2) >> 8;
    output[2] = (x0 + x4) >> 8;
    output[3] = (x8 + x6) >> 8;
    output[4] = (x8 - x6) >> 8;
    output[5] = (x0 - x4) >> 8;
    output[6] = (x3 - x2) >> 8;
    output[7] = (x7 - x1) >> 8;
}

static inline void jrColIdct(int16* input, uint8 *output, int stride) {
    int x0, x1, x2, x3, x4, x5, x6, x7, x8;
    if (!((x1 = input[DCTSIZE * 4] << 8)
        | (x2 = input[DCTSIZE * 6])
        | (x3 = input[DCTSIZE * 2])
        | (x4 = input[DCTSIZE * 1])
        | (x5 = input[DCTSIZE * 7])
        | (x6 = input[DCTSIZE * 5])
        | (x7 = input[DCTSIZE * 3]))) {
        x1 = CLAMP(((input[DCTSIZE * 0] + 32) >> 6) + 128);
        for (x0 = DCTSIZE; x0; --x0) {
            *output = (unsigned char) x1;
            output += stride;
        }
        return;
    }
    x0 = (input[0] << 8) + 8192;
    x8 = W7 * (x4 + x5) + 4;
    x4 = (x8 + (W1 - W7) * x4) >> 3;
    x5 = (x8 - (W1 + W7) * x5) >> 3;
    x8 = W3 * (x6 + x7) + 4;
    x6 = (x8 - (W3 - W5) * x6) >> 3;
    x7 = (x8 - (W3 + W5) * x7) >> 3;
    x8 = x0 + x1;
    x0 -= x1;
    x1 = W6 * (x3 + x2) + 4;
    x2 = (x1 - (W2 + W6) * x2) >> 3;
    x3 = (x1 + (W2 - W6) * x3) >> 3;
    x1 = x4 + x6;
    x4 -= x6;
    x6 = x5 + x7;
    x5 -= x7;
    x7 = x8 + x3;
    x8 -= x3;
    x3 = x0 + x2;
    x0 -= x2;
    x2 = (181 * (x4 + x5) + 128) >> 8;
    x4 = (181 * (x4 - x5) + 128) >> 8;
    *output = CLAMP(((x7 + x1) >> 14) + 128);
    output += stride;
    *output = CLAMP(((x3 + x2) >> 14) + 128);
    output += stride;
    *output = CLAMP(((x0 + x4) >> 14) + 128);
    output += stride;
    *output = CLAMP(((x8 + x6) >> 14) + 128);
    output += stride;
    *output = CLAMP(((x8 - x6) >> 14) + 128);
    output += stride;
    *output = CLAMP(((x0 - x4) >> 14) + 128);
    output += stride;
    *output = CLAMP(((x3 - x2) >> 14) + 128);
    output += stride;
    *output = CLAMP(((x7 - x1) >> 14) + 128);
}

static inline void jr2DIdct(int16 *input, uint8 *output, int16 *tmpStorage, int stride) {
    int i;
    for (i = 0; i < DCTSIZE2; i += DCTSIZE) {
        jrRowIdct(input + i, tmpStorage + i);
    }
    for (i = 0; i < DCTSIZE; ++i) {
        jrColIdct(tmpStorage + i, output + i, stride);
    }
}

static inline int jreadDecodeBlock(jread *reader, jrComp *c, unsigned int mcuX, unsigned int mcuY, unsigned int sampX, unsigned int sampY) {

    jrHuffTbl *ac, *dc;
    struct huffDecStruct *huff;
    int16 *block;
    uint16 *qnt;
    int i;
    int row, col;

    dc = reader->huffTables.dc[c->huffDcTableIndex];
    ac = reader->huffTables.ac[c->huffAcTableIndex];

    qnt = reader->qntTable[c->qntTableIndex];

    if (!dc->prepared) {
        JR_TRY(jreadPrepareHuffTable(reader, dc));
    }

    if (!ac->prepared) {
        JR_TRY(jreadPrepareHuffTable(reader, ac));
    }

    row = (mcuY * c->sampY + sampY);
    col = (mcuX * c->sampX + sampX);
    block = c->coef + (row * DCTSIZE * c->scanlineSize + col * DCTSIZE2);
    memset(block, 0, DCTSIZE2 * sizeof (int16));

    huff = &(reader->huffDec);

    HUFF_DECODE(dc, huff);

    c->dcPredict += huff->value;

    block[0] = c->dcPredict;

    if (reader->_toPhase >= JR_PHASE_ZIG_ZAG) {
        for (i = 1; i < DCTSIZE2; ++i) {
            HUFF_DECODE(ac, huff);

            if (huff->code == 0) {
                break;
            }

            if ((huff->code & 0x0f) == 0 && huff->code != 0xf0) {
                JR_THROW(reader, JR_UNEXPECTED);
            }

            i += huff->code >> 4;

            if (i > DCTSIZE2) {
                JR_THROW(reader, JR_UNEXPECTED);
            }

            block[zigZagRevOrd[i]] = huff->value;
        }
    }
    else {
        for (i = 1; i < DCTSIZE2; ++i) {
            HUFF_DECODE(ac, huff);

            if (huff->code == 0) {
                break;
            }

            if ((huff->code & 0x0f) == 0 && huff->code != 0xf0) {
                JR_THROW(reader, JR_UNEXPECTED);
            }

            i += huff->code >> 4;

            if (i > DCTSIZE2) {
                JR_THROW(reader, JR_UNEXPECTED);
            }

            block[i] = huff->value;
        }
    }

    if (reader->_toPhase >= JR_PHASE_DEQUANT) {
        for (i = 0; i < DCTSIZE2; ++i) {
            block[i] *= qnt[i];
        }
    }

    if (reader->_toPhase >= JR_PHASE_IDCT) {
        int16 tmp[DCTSIZE2];
        jr2DIdct(c->coef + (row * DCTSIZE * c->scanlineSize + col * DCTSIZE2),
                 c->bitmap + (row * DCTSIZE * c->scanlineSize + col * DCTSIZE),
                 tmp, c->scanlineSize);
    }

    return JR_OK;
}

int jreadDecodeScan(jread *reader) {

    unsigned int i, mcuX, mcuY;
    unsigned int sampX, sampY;
    uint16 length, marker;
    uint8 numComps;
    uint8 compId;
    uint8 huffSel;
    uint8 specStart, specEnd, approx;
    int reset, resetI;
    jrComp *c;
    size_t read;

    for (i = 0; i < reader->frame->numOfComponents; ++i) {
        c = reader->frame->component + i;

        c->coef = malloc(sizeof (int16) * DCTSIZE2
                         * c->heightInBlocks
                         * c->widthInBlocks
                         );

        if (c->coef == NULL) {
            JR_THROW(reader, JR_OUT_OF_MEMORY);
        }

        c->bitmap = malloc(sizeof (uint8) * DCTSIZE2
                           * c->heightInBlocks
                           * c->widthInBlocks
                           );

        if (c->bitmap == NULL) {
            JR_THROW(reader, JR_OUT_OF_MEMORY);
        }
    }

    JR_TRY(reader->_readWord(reader, &length));

    FILL_BUFFER(reader, length - 2, &read);

    READ_BYTE(reader, numComps);

    if (reader->frame->numOfComponents != numComps) {
        JR_THROW(reader, JR_UNEXPECTED);
    }

    for (i = 0; i < numComps; ++i) {

        READ_BYTE(reader, compId);

        c = reader->frame->component + compId - 1;

        READ_BYTE(reader, huffSel);

        c->huffDcTableIndex = HIGH_HALF_BYTE(huffSel);
        c->huffAcTableIndex = LOW_HALF_BYTE(huffSel);

    }

    READ_BYTE(reader, specStart);
    READ_BYTE(reader, specEnd);
    READ_BYTE(reader, approx);

    if (specStart != 0 || specEnd != 63 || approx != 0) {
        JR_THROW(reader, JR_UNSUPPORTED);
    }

    reader->huffDec.nbit = 0;

    reset = reader->restartIntervals;
    resetI = 0;

    CLEAR_BUFFER(reader);
    FILL_BUFFER(reader, JR_BUFFER_SIZE, &read);

    for (mcuY = 0; mcuY < reader->frame->heightInMcu; ++mcuY) {
        for (mcuX = 0; mcuX < reader->frame->widthInMcu; ++mcuX) {
            for (i = 0; i < numComps; ++i) {
                c = reader->frame->component + i;
                for (sampY = 0; sampY < c->sampY; ++sampY) {
                    for (sampX = 0; sampX < c->sampX; ++sampX) {
                        if (jreadDecodeBlock(reader, c, mcuX, mcuY, sampX, sampY) == JR_ERR) {
                            if (reader->error == JR_EOF) {
                                reader->error = JR_OK;
                                return JR_OK;
                            }
                            else {
                                return JR_ERR;
                            }
                        }
                    }
                }
            }

            if (reader->restartIntervals && !(--reset)) {
                reader->huffDec.nbit = 0;
                if (BUFFER_SIZE(reader) <= 0) {
                    CLEAR_BUFFER(reader);
                    FILL_BUFFER(reader, JR_BUFFER_SIZE, &read);
                }
                READ_WORD(reader, marker);

                if (marker == JR_EOI) {
                    return JR_OK;
                }
                else if (marker != JR_RST(resetI)) {
                    JR_THROW(reader, JR_UNEXPECTED);
                }

                reset = reader->restartIntervals;
                resetI = (resetI + 1) % 8;

                for (i = 0; i < numComps; ++i) {
                    c = reader->frame->component + i;
                    c->dcPredict = 0;
                }
            }
        }
    }

    return JR_OK;
}

int jreadConvert(jread* reader) {

    jrRgb *im, *px;
    jrComp *c;
    unsigned int x, y;
    int luma, cb, cr;
    int sampX, sampY;

    if ((im = malloc(sizeof (jrRgb) * reader->frame->height * reader->frame->width)) == NULL) {
        JR_THROW(reader, JR_OUT_OF_MEMORY);
    }

    sampX = reader->frame->maxSampX;
    sampY = reader->frame->maxSampY;

    if (reader->frame->numOfComponents == 1) {
        c = reader->frame->component;
        px = im;
        for (y = 0; y < reader->frame->height; ++y) {
            for (x = 0; x < reader->frame->width; ++x) {
                px->r = px->g = px->b = c->bitmap[y * c->scanlineSize + x];
                ++px;
            }
        }
    }
    else {
        for (y = 0; y < reader->frame->height; ++y) {
            for (x = 0; x < reader->frame->width; ++x) {
                c = reader->frame->component;
                luma = c->bitmap[((y * c->sampY) / sampY) * c->scanlineSize + x * c->sampX / sampX] << 8;
                ++c;
                cb = c->bitmap[((y * c->sampY) / sampY) * c->scanlineSize + x * c->sampX / sampX] - 128;
                ++c;
                cr = c->bitmap[((y * c->sampY) / sampY) * c->scanlineSize + x * c->sampX / sampX] - 128;

                px = im + (y * reader->frame->width + x);

                px->r = CLAMP((luma + 359 * cr + 128) >> 8);
                px->g = CLAMP((luma - 88 * cb - 183 * cr + 128) >> 8);
                px->b = CLAMP((luma + 454 * cb + 128) >> 8);
            }
        }
    }

    reader->image = im;

    return JR_OK;

}