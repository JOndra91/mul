/*
 * File: jreadMemory.h
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#ifndef JPEGREADERMEMORY_H
#define    JPEGREADERMEMORY_H

#include <stdint.h>
#include "jread.h"

#ifdef    __cplusplus
extern "C"
{
#endif

    /**
     * Prepares jread for using JPEG memory dump
     * @param reader Pointer to jread structure
     * @param memory Jpeg memory dump
     * @param size Size of jpeg memory dump
     */
    int jreadOpenMemory(jread *reader, void *memory, size_t size);

#ifdef    __cplusplus
}
#endif

#endif    /* JPEGREADERMEMORY_H */

