/*
 * File: jreadDecoders.h
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#ifndef JREADDECODERS_H
#define    JREADDECODERS_H

#include "jread.h"

#ifdef    __cplusplus
extern "C"
{
#endif

    int jreadDecodeScan(jread *reader);
    int jreadConvert(jread *reader);

#ifdef    __cplusplus
}
#endif

#endif    /* JREADDECODERS_H */

