/*
 * File: jreadMacros.h
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#ifndef JPEGREADERMACROS_H
#define    JPEGREADERMACROS_H

#include <stdio.h>

#ifdef    __cplusplus
extern "C"
{
#endif

#define JR_TRY(code) {if((code) != JR_OK) return JR_ERR;}
#define JR_THROW(reader, status) { \
    fprintf(stderr,"%s\n\t%s:%d\n",jrErrStr[status],__FILE__, __LINE__); \
    reader->error = status; \
    return JR_ERR;\
}

#define MAX(a ,b) ((a) > (b) ? (a) : (b))
#define CLAMP(x) ((x > 255) ? 255 : ((x < 0) ? 0 : x))
#define DIV_ROUND_UP(a,b) (((a) + (b) - 1)/(b))

#define HIGH_HALF_BYTE(byte) (byte >> 4)
#define LOW_HALF_BYTE(byte) (byte & 0x0f)

#define BUFFER_SIZE(reader) (((reader)->buffEnd) - ((reader)->buff))

#define DECODE_WORD(word) (word = (word >> 8) | ((word & 0x00ff) << 8))
#define READ_BYTE(reader, dest) do {dest = (*((reader)->buff++));} while(0)
#define READ_WORD(reader, dest) do {dest = (((reader)->buff[0] << 8) | ((reader)->buff[1])); ((reader)->buff += 2);} while(0)

#define CLEAR_BUFFER(reader) do {(reader)->buffEnd = (reader)->buff = (reader)->_buffPtr; } while(0)

#define FILL_BUFFER(reader, length, read) do{ \
    JR_TRY((reader)->_read(reader, (reader)->buff, (length), (read))); \
    (reader)->buffEnd = (reader)->buff + *(read); \
} while(0)


#ifdef    __cplusplus
}
#endif

#endif    /* JPEGREADERMACROS_H */

