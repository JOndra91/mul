/*
 * File: shader_vertex.glsl
 *
 * Copyright (C) 2014 Ondřej Janošík
 *
 * This file is part of Ogljpeg.
 *
 *   Ogljpeg is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as
 *   published by the Free Software Foundation, either version 3 of
 *   the License, or (at your option) any later version.
 *
 *   Ogljpeg is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the Lesser GNU General Public License
 *   along with Ogljpeg.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#version 120

attribute vec3 coord3d;
attribute vec2 coordTex;

varying vec2 fCoordTex;

void main() {
    gl_Position = vec4(coord3d, 1.0);
    fCoordTex = coordTex;
}
