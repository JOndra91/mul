/*
 * File: jreadMarkers.h
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#ifndef JPEGMARKERS_H
#define    JPEGMARKERS_H

#include "jread.h"

#ifdef    __cplusplus
extern "C"
{
#endif

    int jreadGetQntTable(jread *reader);
    int jreadGetHuffTable(jread *reader);
    int jreadGetRstDef(jread *reader);
    int jreadGetNumLines(jread *reader);
    int jreadGetFrame0(jread *reader);
    int jreadSkip(jread *reader);


#ifdef    __cplusplus
}
#endif

#endif    /* JPEGMARKERS_H */

