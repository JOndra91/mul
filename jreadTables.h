/*
 * File: jreadTables.h
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *
 */

#ifndef JREADTABLES_H
#define    JREADTABLES_H

#include "jread.h"

#ifdef    __cplusplus
extern "C"
{
#endif

    extern const int zigZagOrd[DCTSIZE2];
    extern const int zigZagRevOrd[DCTSIZE2];

#ifdef    __cplusplus
}
#endif

#endif    /* JREADTABLES_H */

