/*
 * File: shaderUtils.c
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *
 */

#include <string.h>
#include <stdlib.h>
#include "shaderUtils.h"

t_ShaderUtilsError shaderUtilsErrno;
char *shaderUtilsError = NULL;

void setGLError(GLuint object);
void setCustomError(const char *err);

void shaderUtilsPrintGLError(GLuint object) {
    setGLError(object);
    shaderUtilsPrintError(NULL);
}

unsigned char* getFileContent(const char* path, unsigned long *length) {
    FILE *f;
    unsigned char *content;
    unsigned long size;
    if ((f = fopen(path, "r")) != NULL) {
        fseek(f, 0, SEEK_END);
        size = ftell(f);
        fseek(f, 0, SEEK_SET);

        if (length) {
            *length = size + 1;
        }

        content = (unsigned char*) malloc((size + 1) * sizeof (unsigned char));

        if (content) {
            if (fread(content, sizeof (char), size, f) != size) {
                fclose(f);
                return NULL;
            }
            fclose(f);

            content[size] = 0;

            return content;
        }

        fclose(f);
    }

    return NULL;
}

void setGLError(GLuint object) {
    GLint len = 0;

    if (glIsShader(object)) {
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &len);
    }
    else if (glIsProgram(object)) {
        glGetProgramiv(object, GL_INFO_LOG_LENGTH, &len);
    }
    else {
        return;
    }

    if (shaderUtilsError != NULL) {
        free(shaderUtilsError);
    }

    shaderUtilsError = (char*) malloc(len * sizeof (char));

    if (glIsShader(object)) {
        glGetShaderInfoLog(object, len, NULL, shaderUtilsError);
    }
    else if (glIsProgram(object)) {
        glGetProgramInfoLog(object, len, NULL, shaderUtilsError);
    }
}

void setCustomError(const char *err) {
    int len = strlen(err);

    if (shaderUtilsError != NULL) {
        free(shaderUtilsError);
    }

    shaderUtilsError = (char*) malloc(len * sizeof (char));

    if (shaderUtilsError) {
        strcpy(shaderUtilsError, err);
    }
    else {
        shaderUtilsError = "Internal error";
    }

}

void shaderUtilsPrintError(char *info) {
    if (shaderUtilsError == NULL) {
        fprintf(stderr, "%s\n", "No OpenGL error");
    }
    else if (info != NULL) {
        fprintf(stderr, "%s\n\t%s\n", info, shaderUtilsError);
    }
    else {
        fprintf(stderr, "%s\n", shaderUtilsError);
    }
}

GLuint createShaderFromFile(const char *sourceFile, GLenum type) {
    GLchar *source = (GLchar*) getFileContent(sourceFile, NULL);
    GLuint res;
    char err[512];

    if (source == NULL) {
        sprintf(err, "Can't open file \"%s\"", sourceFile);
        setCustomError(err);
        shaderUtilsErrno = ERR_NO_FILE;
        return 0;
    }

    res = createShaderFromSource(source, type);

    free(source);

    return res;
}

GLuint createShaderFromSource(const char *source, GLenum type) {
    GLint compileStatus;
    GLuint res = glCreateShader(type);

    glShaderSource(res, 1, (const GLchar**) (&source), NULL);

    glCompileShader(res);
    glGetShaderiv(res, GL_COMPILE_STATUS, &compileStatus);

    if (!compileStatus) {
        setGLError(res);
        if (type == GL_VERTEX_SHADER) {
            shaderUtilsErrno = ERR_VERTEX_SHADER;
        }
        else if (type == GL_FRAGMENT_SHADER) {
            shaderUtilsErrno = ERR_FRAGMENT_SHADER;
        }
        else if (type == GL_COMPUTE_SHADER) {
            shaderUtilsErrno = ERR_COMPUTE_SHADER;
        }
        glDeleteShader(res);
        return 0;
    }

    return res;
}

GLuint compileProgramFromFile(const char *vertexShaderFile, const char *fragmentShaderFile) {
    GLint linkState;
    GLuint vs, fs, program;

    if ((vs = createShaderFromFile(vertexShaderFile, GL_VERTEX_SHADER)) == 0) {
        return 0;
    }

    if ((fs = createShaderFromFile(fragmentShaderFile, GL_FRAGMENT_SHADER)) == 0) {
        glDeleteShader(vs);
        return 0;
    }

    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &linkState);

    if (!linkState) {
        setGLError(program);
        shaderUtilsErrno = ERR_PROGRAM_LINK;
        glDeleteShader(vs);
        glDeleteShader(fs);
        return 0;
    }

    return program;
}

GLuint compileProgramFromSource(const char *vertexShaderSource, const char *fragmentShaderSource) {
    GLint linkState;
    GLuint vs, fs, program;

    if ((vs = createShaderFromSource(vertexShaderSource, GL_VERTEX_SHADER)) == 0) {
        return 0;
    }

    if ((fs = createShaderFromSource(fragmentShaderSource, GL_FRAGMENT_SHADER)) == 0) {
        glDeleteShader(vs);
        return 0;
    }

    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &linkState);

    if (!linkState) {
        setGLError(program);
        shaderUtilsErrno = ERR_PROGRAM_LINK;
        glDeleteShader(vs);
        glDeleteShader(fs);
        return 0;
    }

    return program;
}

GLuint compileComputeProgramFromFile(const char *computeShaderFile) {
    GLint linkState;
    GLuint cs, program;

    if ((cs = createShaderFromFile(computeShaderFile, GL_COMPUTE_SHADER)) == 0) {
        return 0;
    }

    program = glCreateProgram();
    glAttachShader(program, cs);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &linkState);

    if (!linkState) {
        setGLError(program);
        shaderUtilsErrno = ERR_PROGRAM_LINK;
        glDeleteShader(cs);
        return 0;
    }

    return program;
}

GLuint compileComputeProgramFromSource(const char *source) {
    GLint linkState;
    GLuint cs, program;

    if ((cs = createShaderFromSource(source, GL_COMPUTE_SHADER)) == 0) {
        return 0;
    }

    program = glCreateProgram();
    glAttachShader(program, cs);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &linkState);

    if (!linkState) {
        setGLError(program);
        shaderUtilsErrno = ERR_PROGRAM_LINK;
        glDeleteShader(cs);
        return 0;
    }

    return program;
}