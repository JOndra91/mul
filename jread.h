/*
 * File: jread.h
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#ifndef JPEGREADER_H
#define    JPEGREADER_H

#include <stdlib.h>

#ifdef    __cplusplus
extern "C"
{
#endif

#define JR_MARKER 0xff
#define JR_ZERO 0xff00
#define JR_SOF0 0xffc0
#define JR_SOF(n) (JR_SOF0 + n)
#define JR_DHT 0xffc4
#define JR_DAC 0xffcc
#define JR_RST0 0xffd0
#define JR_RST(n) (JR_RST0 + n)
#define JR_SOI 0xffd8
#define JR_EOI 0xffd9
#define JR_SOS 0xffda
#define JR_DQT 0xffdb
#define JR_DNL 0xffdc
#define JR_DRI 0xffdd
#define JR_APP0 0xffe0
#define JR_APP(n) (JR_APP0 + 0)
#define JR_COM 0xfffe

#define JR_FALSE 0
#define JR_TRUE 1

#define IS_MARKER(w) ((w & 0xff00) == 0xff00)
#define IS_ZERO_MARKER(w) (w == JR_ZERO)
#define IS_SOF_MARKER(w) ((w & 0xff00) == JR_SOF0)

#define DCTSIZE 8
#define DCTSIZE2 64
#define JR_BUFFER_SIZE 8192

#define NUM_QNT_TABLES 4
#define NUM_HUFF_TABLES 2
#define HUFF_NODE 0
#define HUFF_LEAF 1

#define HUFF_LOOKUP_BITS 16
#define HUFF_LOOKUP_SIZE (1 << HUFF_LOOKUP_BITS)

    typedef unsigned char uint8;
    typedef unsigned short uint16;
    typedef char int8;
    typedef short int16;

    typedef struct struct_jread jread;
    typedef struct struct_jrComp jrComp;

    typedef int (jrRead) (jread *reader, void *dest, size_t n, size_t *read);
    typedef int (jrReadWord) (jread *reader, uint16 *dest);
    typedef int (jrSeek) (jread *reader, size_t offset);
    typedef void (jrClose) (jread *reader);
    typedef void (jrInfo) (jread *reader, size_t *currOffset, size_t *size);

    extern char *jrErrStr[];

    typedef enum
    {
        JR_OK = 0,
        JR_ERR,
        JR_OUT_OF_MEMORY,
        JR_UNEXPECTED,
        JR_UNSUPPORTED,
        JR_EOF
    } jrStatus;

    typedef enum
    {
        JR_PHASE_HUFF = 0,
        JR_PHASE_ZIG_ZAG,
        JR_PHASE_DEQUANT,
        JR_PHASE_IDCT,
        JR_PHASE_CONVERT,
        JR_PHASE_ALL
    } jrPhase;

    struct struct_jrComp
    {
        uint8 index;
        uint8 sampX;
        uint8 sampY;
        uint8 qntTableIndex;
        uint8 huffDcTableIndex;
        uint8 huffAcTableIndex;
        uint8 *bitmap;
        int16 *coef;
        int dcPredict;
        uint16 scanlineSize;
        int heightInBlocks;
        int widthInBlocks;
    };

    typedef struct
    {
        uint8 precision;
        uint16 height;
        uint16 width;
        uint8 numOfComponents;
        jrComp component[3];
        int mcuHeight;
        int mcuWidth;
        int heightInMcu;
        int widthInMcu;
        int heightInBlocks;
        int widthInBlocks;
        int maxSampX;
        int maxSampY;
    } jrFrame;

    typedef struct
    {
        int16 type;
        int16 value;
    } jrHuffNode;

    typedef struct
    {
        uint8 length;
        uint8 code;
    } jrHuffLookup;

    typedef struct
    {
        uint8 bits[16];
        uint8 vals[256];
        jrHuffNode *tree;
        jrHuffLookup *lookupTable;
        int prepared;
    } jrHuffTbl;

    typedef struct
    {
        uint8 r;
        uint8 g;
        uint8 b;
    } jrRgb;

    struct struct_jread
    {
        void *_source;
        uint8 *_buffPtr;
        uint8 *buffEnd;
        uint8 *buff;
        jrRead *_read;
        jrReadWord *_readWord;
        jrSeek *_seek;
        jrInfo *_info;
        jrClose *_close;
        jrPhase _toPhase;
        jrFrame *frame;
        int restartIntervals;
        int numberOfLines;
        jrStatus error;
        uint16 *qntTable[NUM_QNT_TABLES];

        struct
        {
            jrHuffTbl *ac[NUM_HUFF_TABLES];
            jrHuffTbl *dc[NUM_HUFF_TABLES];
        } huffTables;

        struct huffDecStruct
        {
            int nbit;
            uint16 code;
            unsigned int bits;
            int value;
        } huffDec;

        jrRgb *image;

        struct huffCodedBlockMap
        {
            unsigned int offset;
            unsigned int size;
        } *blockMap;
        uint8 *blocks;
        size_t blocksSize;
    };

    /**
     * Initialize jread structure
     * This function is called internally
     */
    void jreadInit(jread *reader);

    int jreadGetHeaders(jread *reader);
    int jreadDecode(jread* reader);

    /**
     * Release resources allocated by decompression
     * @param reader Pointer to jread structure
     */
    void jreadFree(jread *reader);

#include "jreadMemory.h"

#ifdef    __cplusplus
}
#endif

#endif    /* JPEGREADER_H */

