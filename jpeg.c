/*
 * File: jpeg.c
 *
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 *         Michal Pazdera <xpazde12@stud.fit.vutbr.cz>
 *
 */

#include <GL/glew.h>
#include <GL/freeglut_std.h>
#include <GL/freeglut_ext.h>
#include <GL/glx.h>
#include <stdio.h>

#include "shaderUtils.h"
#include "jread.h"

extern char *shader_fragment;
extern char *shader_vertex;

#define FILTERING GL_LINEAR

GLuint textureId;
GLuint glDisplay;

void reshapeFunc(GLint newWidth, GLint newHeight);
int initResources();
void freeResources();

GLint attrTexCoord, attrCoord;
GLint uniformJpeg;
GLint winWidth, winHeight;
GLuint vboPlane, vboTexCoord;
GLuint iboPlaneElements;

char *jpegImage = NULL;
unsigned char *jpegImageBuffer = NULL;
unsigned long jpegImageBufferSize;

int renderCount = 0;

jread reader;

void reshapeFunc(GLint newWidth, GLint newHeight) {
    winWidth = newWidth;
    winHeight = newHeight;

    glutPostRedisplay();
}

void displayFunc() {

    glViewport(0, 0, winWidth, winHeight);

    glUseProgram(glDisplay);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);

    glUniform1i(uniformJpeg, /*GL_TEXTURE0*/ 0);

    glBindBuffer(GL_ARRAY_BUFFER, vboTexCoord);
    glEnableVertexAttribArray(attrTexCoord);
    glVertexAttribPointer(
                          attrTexCoord,
                          2,
                          GL_FLOAT,
                          GL_FALSE,
                          0,
                          0);

    glBindBuffer(GL_ARRAY_BUFFER, vboPlane);
    glEnableVertexAttribArray(attrCoord);
    glVertexAttribPointer(
                          attrCoord,
                          3,
                          GL_FLOAT,
                          GL_FALSE,
                          0,
                          0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboPlaneElements);

    int size;
    glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
    glDrawElements(GL_TRIANGLES, size / sizeof (GLushort), GL_UNSIGNED_SHORT, 0);

    glFinish();
    glutSwapBuffers();

    glDisableVertexAttribArray(attrCoord);
    glDisableVertexAttribArray(attrTexCoord);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    // Better program startup because
    // empty (transparent) window looks stupid
    if (renderCount == 0) {
        glutPostRedisplay();
        renderCount++;
    }
    else if (renderCount == 1) {
        showImage(jpegImage);
        glutPostRedisplay();
        renderCount++;
    }
}

int initResources() {

    GLint major, minor;

    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);

    if (major < 2 || (major == 2 && minor < 1)) {
        fprintf(stderr, "%s\n", "At least OpenGL 2.1 is required");
        return EXIT_FAILURE;
    }

    glDisplay = compileProgramFromSource(shader_vertex, shader_fragment);

    if (glDisplay == 0) {
        shaderUtilsPrintError(NULL);
        return EXIT_FAILURE;
    }

    glUseProgram(glDisplay);

    attrCoord = glGetAttribLocation(glDisplay, "coord3d");
    if (attrCoord == -1) {
        shaderUtilsPrintGLError(glDisplay);
        return EXIT_FAILURE;
    }

    if ((attrTexCoord = glGetAttribLocation(glDisplay, "coordTex")) == -1) {
        shaderUtilsPrintGLError(glDisplay);
        return EXIT_FAILURE;
    }

    if ((uniformJpeg = glGetUniformLocation(glDisplay, "jpegTex")) == -1) {
        shaderUtilsPrintGLError(glDisplay);
        return EXIT_FAILURE;
    }

    glGenTextures(1, &textureId);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, FILTERING);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, FILTERING);

    glTexImage2D(
                 GL_TEXTURE_2D,
                 0,
                 GL_RGB,
                 1,
                 1,
                 0,
                 GL_RGB,
                 GL_UNSIGNED_BYTE,
                 NULL);

    glBindTexture(GL_TEXTURE_2D, GL_NONE);

    glGenBuffers(1, &vboTexCoord);
    glBindBuffer(GL_ARRAY_BUFFER, vboTexCoord);

    GLfloat texCoords[] = {
        0.0, 1.0,
        1.0, 1.0,
        1.0, 0.0,
        0.0, 0.0,
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof (texCoords), texCoords, GL_STATIC_DRAW);

    glGenBuffers(1, &vboPlane);
    glBindBuffer(GL_ARRAY_BUFFER, vboPlane);

    GLfloat planeVerticies[] = {
        -1.0, -1.0, 0.0,
        1.0, -1.0, 0.0,
        1.0, 1.0, 0.0,
        -1.0, 1.0, 0.0,
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof (planeVerticies), planeVerticies, GL_STATIC_DRAW);

    glGenBuffers(1, &iboPlaneElements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboPlaneElements);

    GLushort planeElements[] = {
        0, 1, 2,
        2, 3, 0,
    };

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof (planeElements), planeElements, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return EXIT_SUCCESS;
}

int getImage(char *imageFile) {

    if ((jpegImageBuffer = getFileContent(imageFile, &jpegImageBufferSize)) == NULL) {
        fprintf(stderr, "Could not open file \"%s\"\n", imageFile);
        return EXIT_FAILURE;
    }

    if (jreadOpenMemory(&reader, jpegImageBuffer, jpegImageBufferSize) != JR_OK) {
        return EXIT_FAILURE;
    }

    if (jreadGetHeaders(&reader) != JR_OK || jreadDecode(&reader) != JR_OK) {
        fprintf(stderr, "Unsupported format or corrupted file \"%s\"\n", imageFile);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int showImage(char *imageFile) {

    getImage(imageFile); // fills jread renderer

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);

    glTexImage2D(
                 GL_TEXTURE_2D,
                 0,
                 GL_RGB,
                 reader.frame->width,
                 reader.frame->height,
                 0,
                 GL_RGB,
                 GL_UNSIGNED_BYTE,
                 reader.image);

    glBindTexture(GL_TEXTURE_2D, GL_NONE);

    jreadFree(&reader);
}

void freeResources() {

    glDeleteTextures(1, &textureId);

    glDeleteBuffers(1, &vboTexCoord);
    glDeleteBuffers(1, &vboPlane);
    glDeleteBuffers(1, &iboPlaneElements);

    if (jpegImageBuffer) {
        free(jpegImageBuffer);
    }
}

int main(int argc, char** argv) {

    if (argc == 2) { // use opengl
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
        glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
        glutInitWindowSize(800, 400);
        glutCreateWindow("Jpeg viewer");

        if (glewInit() != GLEW_OK) {
            return EXIT_FAILURE;
        }

        jpegImage = argv[1];

        if (initResources() == EXIT_FAILURE) {
            fprintf(stderr, "%s\n", "Could not initialize all resources.");
            return EXIT_FAILURE;
        }

        glutDisplayFunc(displayFunc);
        glutReshapeFunc(reshapeFunc);

        glutMainLoop();

        freeResources();
    } else if (argc == 3) { // store to ppm
        FILE *outFile;

        jpegImage = argv[1];
        getImage(jpegImage);

        if ((outFile = fopen(argv[2], "wb")) == NULL) {
            fprintf(stderr, "Could not open file \"%s\"\n", argv[2]);
        }

        fprintf(outFile, "P6\n%d %d\n255\n", reader.frame->width, reader.frame->height);
        fwrite(reader.image, sizeof(jrRgb)*reader.frame->width*reader.frame->height, 1, outFile);

        fclose(outFile);

        jreadFree(&reader);

    } else { // bad parameters
        fprintf(stderr, "%s\n", "Bad input parameters: ");
        fprintf(stderr, "%s\n", "Use \t\"./jpeg <infile>\" to display image or");
        fprintf(stderr, "%s\n", "\t\"./jpeg <infile> <outfile>\" to save image into ppm format");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


